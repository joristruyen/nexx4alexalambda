const hello = async () => {
  // successResponse handles wrapping the response in an API Gateway friendly
  // format (see other responses, including CORS, in `./utils/lambda-response.js)
  console.log('Hello there');
};

// runWarm function handles pings from the scheduler so you don't
// have to put that boilerplate in your function.
export default hello;
